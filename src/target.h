#pragma once
#include <string>
#include <vector>

namespace autotools {
    class Makefile;
    
    class Target {
        friend class Makefile;
        public:
        enum Types
        {
            program,
            library,
        };

    private:
        Makefile& parent;
        std::string name, prefix;
        Types type;
        std::vector<std::string> sources, ldflags, ldadd, libadd, cppflags, cflags;

    public:
        Target(Makefile& parent);
        std::string getName();
        std::vector<std::string> getSources();
        std::vector<std::string> getLDADD();
        std::vector<std::string> getLIBADD();
        std::vector<std::string> getCFLAGS();
        std::vector<std::string> getCPPFLAGS();
        std::vector<std::string> getLDFLAGS();
        Types getType();

        void addSources(std::vector<std::string> srcs);
        void clearSources();
        
        void clearCPPFLAGS();
        void addCPPFLAGS(std::vector<std::string> flags);

        void clearCFLAGS();
        void addCFLAGS(std::vector<std::string> flags);

        void clearLDFLAGS();
        void addLDFLAGS(std::vector<std::string> flags);
        
        void clearLDADD();
        void addLDADD(std::vector<std::string> flags);

        void clearLIBADD();
        void addLIBADD(std::vector<std::string> flags);
    };
}
