#pragma once
#include <string>
#include <memory>
#include "target.h"



namespace autotools {
    typedef std::shared_ptr<autotools::Target> Target_ref;
    class Configurator;

    class Makefile {
        friend class Configurator;
    private:
        std::shared_ptr<autotools::Configurator> parent;
        std::string path,config;
        std::vector<Target_ref> targets;
        std::vector<std::string> extradist,includeheaders,pkgincludeheaders,subdirs,pkgconfig;
        std::string makefileam;
        
        void searchTargets();
        void searchSubdir();
        void searchExtradist();
        void searchIncludes();
        void searchPkgIncludes();
        void searchPkgConfig();
    public:
        Makefile(std::shared_ptr<autotools::Configurator> parent, std::string path);
        std::string getPath();
        void setPath(std::string path);
        std::string getConfig();
        std::vector<Target_ref> getTargets();
        Target_ref addTarget(std::string name, Target::Types type, std::string prexix);
        void addSubdir(std::string path);
        void addPkgConfig(std::string path);

        void addIncludes(std::vector<std::string> lst);
        void addPkgIncludes(std::vector<std::string> lst);
        void addExtradist(std::vector<std::string> lst);

        std::vector<std::string> getSubdirs();

        void delSubdir(std::string subdir);

        void Write();
    };
}
