#include "makefile.h"
#include "configurator.h"
#include <filesystem>
#include <fstream>
#include <iostream>
#include "stringops.h"
#include  <map>
#include <algorithm>

using namespace autotools;
using namespace std;

Makefile::Makefile(std::shared_ptr<autotools::Configurator> parent, string path) : parent(parent), path(path)
{
    this->config = path + ".am";
}

std::string Makefile::getPath()
{
    return this->path;
}

void Makefile::setPath(std::string path)
{
    this->path = path;
    this->config = path + ".am";
}

std::string Makefile::getConfig()
{
    return this->config;
}

void Makefile::searchTargets()
{
    auto makefilepath = parent->getProjectPath() + "/" + this->config;

    if (std::filesystem::is_regular_file(makefilepath))
    {
        this->makefileam = string_from_file(makefilepath);

        auto programs = genlistprefix(makefileam, "_PROGRAMS");
        auto ltlibs = genlistprefix(makefileam, "_LTLIBRARIES");

        extradist = genlist(makefileam, "EXTRA_DIST");
        includeheaders = genlist(makefileam, "include_HEADERS");

        for (auto &prog : programs)
        {
            string searchname = dotconvert(prog.name);
            Target_ref trg = make_shared<Target>(*this);
            trg->sources = genlist(makefileam, searchname + "_SOURCES");
            trg->cflags = genlist(makefileam, searchname + "_CFLAGS");
            trg->cppflags = genlist(makefileam, searchname + "_CPPFLAGS");
            trg->ldadd = genlist(makefileam, searchname + "_LDADD");
            trg->libadd = genlist(makefileam, searchname + "_LIBADD");
            trg->ldflags = genlist(makefileam, searchname + "_LDFLAGS");
            trg->name = prog.name;
            trg->prefix = prog.prefix;
            trg->type = Target::Types::program;
            this->targets.push_back(trg);
        }

        for (auto &ltlib : ltlibs)
        {
            string searchname = dotconvert(ltlib.name);
            Target_ref trg = make_shared<Target>(*this);
            trg->name = ltlib.name;
            trg->prefix = ltlib.prefix;
            trg->type = Target::Types::library;
            trg->sources = genlist(makefileam, searchname + "_SOURCES");
            trg->cflags = genlist(makefileam, searchname + "_CFLAGS");
            trg->cppflags = genlist(makefileam, searchname + "_CPPFLAGS");
            trg->ldadd = genlist(makefileam, searchname + "_LDADD");
            trg->libadd = genlist(makefileam, searchname + "_LIBADD");
            trg->ldflags = genlist(makefileam, searchname + "_LDFLAGS");
            this->targets.push_back(trg);
        }
    }
    else
    {
    }
}

std::vector<Target_ref> Makefile::getTargets()
{
    return this->targets;
}

Target_ref Makefile::addTarget(std::string name, Target::Types type, std::string prefix)
{
    Target_ref tg = make_shared<Target>(*this);
    tg->name = name;
    tg->type = type;
    tg->prefix = prefix;
    this->targets.push_back(tg);
    return tg;
}

void Makefile::Write()
{
    makefileam = "";
    vector<string> bin_PROGRAMS, noinst_PROGRAMS, lib_LTLIBRARIES;
    std::map<string, std::vector<std::string>> pmap;
    std::map<string, std::vector<std::string>> lmap;

    for (auto tg : targets) {
        auto & pfix = tg->prefix;
        auto & name = tg->name;
        switch(tg->type)
        {
            case Target::program:
                pmap[pfix].push_back(name);
            break;
            case Target::library:
                lmap[pfix].push_back(name);
            break;
        }
    }

    for (auto keys: pmap) {
        string key = keys.first;
        auto pls = keys.second;
        makefileam +=  key + "_PROGRAMS = ";
        for (auto &s : pls)
        {
            makefileam += s + " ";
        }
        makefileam += "\n";
    }


    for (auto keys: lmap) {
        string key = keys.first;
        auto pls = keys.second;
        makefileam +=  key + "_LTLIBRARIES = ";
        for (auto &s : pls)
        {
            makefileam += s + " ";
        }
        makefileam += "\n";
    }

    for (auto tg : targets)
    {
        switch (tg->type)
        {
        case Target::Types::program:
            bin_PROGRAMS.push_back(tg->name);
            break;
        case Target::Types::library:
            lib_LTLIBRARIES.push_back(tg->name);
            break;
        default:
        break;
        }
    }

    for (auto tg : targets)
    {
        string tgname = dotconvert(tg->name);
        if (tg->sources.size() > 0)
        {
            makefileam += tgname + "_SOURCES = ";
            for (auto src : tg->sources)
                makefileam += src + " ";
            makefileam += "\n";
        }

        if (tg->cppflags.size() > 0)
        {
            makefileam += tgname + "_CPPFLAGS = ";
            for (auto flag : tg->cppflags)
                makefileam += flag + " ";
            makefileam += "\n";
        }

        if (tg->cflags.size() > 0)
        {
            makefileam += tgname + "_CLAGS = ";
            for (auto flag : tg->cflags)
                makefileam += flag + " ";
            makefileam += "\n";
        }

        if (tg->ldflags.size() > 0)
        {
            makefileam += tgname + "_LDFLAGS = ";
            for (auto flag : tg->ldflags)
                makefileam += flag + " ";
            makefileam += "\n";
        }

        if (tg->ldadd.size() > 0)
        {
            makefileam += tgname + "_LDADD = ";
            for (auto flag : tg->ldadd)
                makefileam += flag + " ";
            makefileam += "\n";
        }
        if (tg->libadd.size() > 0)
        {
            makefileam += tgname + "_LIBADD = ";
            for (auto flag : tg->libadd)
                makefileam += flag + " ";
            makefileam += "\n";
        }
        //TODO ldadd libadd
    }
    if (this->subdirs.size() > 0) {
        makefileam += "SUBDIRS = ";
        makefileam +=  vec2str(this->subdirs," ") + "\n";
    }

    if (this->pkgconfig.size() > 0) {
        makefileam += "pkgconfig_DATA = ";
        makefileam +=  vec2str(this->pkgconfig," ") + "\n";
    }

    if (this->includeheaders.size() > 0) {
        makefileam += "include_HEADERS = ";
        makefileam +=  vec2str(this->includeheaders," ") + "\n";
    }

    if (this->pkgincludeheaders.size() > 0) {
        makefileam += "pkginclude_HEADERS = ";
        makefileam +=  vec2str(this->pkgincludeheaders," ") + "\n";
    }

    if (this->extradist.size() > 0) {
        makefileam += "EXTRA_DIST = ";
        makefileam +=  vec2str(this->extradist," ") + "\n";
    }

    string makefile_am = parent->getProjectPath() + "/" + this->config;
    int found = makefile_am.find("Makefile.am");
    string dirpath = makefile_am.substr(0,found);
    if (! std::filesystem::is_directory(dirpath)) {
        if (!std::filesystem::create_directory(dirpath))
            throw  std::runtime_error("Cant create directory");
    }
    ofstream out(makefile_am);
    out << makefileam;
    out.close();
}


void Makefile::addSubdir(string path) {
    auto found = std::find(this->subdirs.begin(),this->subdirs.end(), path);
    if (found == this->subdirs.end())
        this->subdirs.push_back(path);
    else
        throw std::runtime_error("Item in list exists");
}

void Makefile::addPkgConfig(string pcname) {
    this->pkgconfig.push_back(pcname);
}

void Makefile::searchSubdir() {
    this->subdirs = genlist(makefileam, "SUBDIRS");
}

void Makefile::searchPkgConfig() {
    this->pkgconfig = genlist(makefileam, "pkgconfig_DATA");
}

void Makefile::searchExtradist() {
    this->extradist = genlist(makefileam, "EXTRA_DIST");
}

void Makefile::searchIncludes() {
    this->includeheaders = genlist(makefileam, "include_HEADERS");
}

void Makefile::searchPkgIncludes() {
    this->pkgincludeheaders = genlist(makefileam, "pkginclude_HEADERS");
}


void Makefile::addIncludes(std::vector<std::string> lst){
    this->includeheaders.insert(this->includeheaders.end(),lst.begin(),lst.end());
}
void Makefile::addPkgIncludes(std::vector<std::string> lst){
    this->pkgincludeheaders.insert(this->pkgincludeheaders.end(),lst.begin(),lst.end());
}
void Makefile::addExtradist(std::vector<std::string> lst){
    this->extradist.insert(this->extradist.end(),lst.begin(),lst.end());
}

std::vector<std::string> Makefile::getSubdirs()
{
    return  this->subdirs;
}

void Makefile::delSubdir(std::string subdir)
{
    auto found = std::find(subdirs.begin(), subdirs.end(), subdir);
    if (found != subdirs.end()) {
        subdirs.erase(found);
    }
}