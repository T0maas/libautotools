#pragma once
#include <string>
#include <vector>
#include <memory>
#include "makefile.h"
#include "pcfile.h"

namespace autotools
{
    typedef std::shared_ptr<autotools::Makefile> Makefile_ref;
    typedef std::shared_ptr<autotools::PCFile> PCFile_ref;

    struct Library_check {
        std::string lib, func;
        std::string found, notfound;
    };

    struct Header_check {
        std::string header;
        std::string found, notfound;
    };

     struct Module_check {
        std::string prefix, name;
    };

    class Configurator : public std::enable_shared_from_this<Configurator>
    {
    private:
        Configurator() = default;
    protected:
        std::string configureac;
        std::vector<std::string> cfgs;
        std::vector<Makefile_ref> makefiles;
        std::vector<PCFile_ref> PCfiles;
        std::string projectpath,name,version,mail,auxdir;
        std::vector<Library_check> libs;
        std::vector<Header_check> headers;
        std::vector<Module_check> modules;
        bool cxx;
        bool am_maintainer_mode;

        void searchCFGs();
        void searchMakefiles();
        void searchPCFiles();
        void getInit();

        void searchCheckLib();
        void searchCheckHeader();
        void searchModules();

    public:
        static std::shared_ptr<Configurator> OpenProject(std::string path);
        static std::shared_ptr<Configurator> CreateProject(std::string path, std::string name="", std::string version="", std::string mail="");

        std::vector<Makefile_ref> getMakefiles();
        std::vector<PCFile_ref> getPCFiles();

        void setProjectPath(std::string  path);
        std::string getProjectPath();
        
        autotools::Makefile_ref addMakefile(std::string path);
        void delMakefile(autotools::Makefile_ref mkf);
        autotools::PCFile_ref addPCfile(std::string path);

        void setVersion(std::string ver);
        std::string getVersion();

        void setName(std::string name);
        std::string getName();

        void setMail(std::string mail);
        std::string getMail();

        void setAuxdir(std::string arg);
        std::string getAuxdir();

        void enableCXX(bool tf);
        bool getCXX();

        void addCheckLib(std::string lib, std::string func, std::string found, std::string nfound);
        void addCheckHeader(std::string head, std::string found, std::string nfound);
        void addModule(std::string prefix, std::string name);
        void Write();
        std::vector<Header_check> getCheckHeaders();
        std::vector<Library_check> getCheckLibraries();
        std::vector<Module_check> getCheckModules();

        Header_check& getHeader(int index);
        Library_check& getLibrary(int index);
        Module_check& getModule(int index);

        void delHeader(int index);
        void delLib(int index);
        void delModule(int index);
        
    };
}
