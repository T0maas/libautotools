#include "pcfile.h"
#include <fstream>
#include <sstream>
#include "configurator.h"

using namespace autotools;

PCFile::PCFile(autotools::Configurator & parent) : parent(parent) {

}

std::string PCFile::getPath() {
    return this->path;
}

std::string PCFile::getConfig() {
    return this->config;
}

std::string PCFile::getContent()
{
    return  this->content;
}

void PCFile::setContent(std::string str)
{
    this->content = str;
}

void PCFile::Write()
{
    std::string path = parent.getProjectPath() + "/" + this->config;
    std::ofstream out(path);
    out << this->content;
    out.close();
}



