#pragma once

#include <string>
#include <vector>


extern void remove_whitespace(std::string & str);
extern std::vector<std::string> string_split(const std::string & str, const std::string & delim);
extern bool string_contains(const std::string & str, const std::string & substr);
extern std::vector<std::string> genlist(std::string in, std::string key);

struct PrefLst {
    std::string prefix, name;
};

extern std::vector<PrefLst> genlistprefix(std::string in, std::string key);

extern std::string dotconvert(std::string dotname);
extern std::string string_from_file(std::string path);
extern std::vector<std::string> getconfigslist(std::string configureac, std::string cfgname);
extern int findClosingParen(std::string text, int openPos, char parentype);
extern std::string extractParen(std::string text);
extern std::string extractParen2(std::string text);
extern std::vector<int> string_findall(std::string str, std::string search);
extern std::string vec2str(std::vector<std::string> vec, std::string sep);