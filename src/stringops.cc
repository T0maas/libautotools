#include "stringops.h"
#include <algorithm>
#include <fstream>
#include <filesystem>
#include <sstream>


using namespace std;

void remove_whitespace(string & str) {
    for (int toremove = str.find("\n"); toremove > -1; toremove = str.find("\n")) {
        str.replace(toremove, 1, " ");
    }
    for (int toremove = str.find("\r"); toremove > -1; toremove = str.find("\r")) {
        str.replace(toremove, 1, " ");
    }
    for (int toremove = str.find("\t"); toremove > -1; toremove = str.find("\t")) {
        str.replace(toremove, 1, " ");
    }

    for (int toremove = str.find("  "); toremove > -1; toremove = str.find("  ")) {
        str.replace(toremove, 2, " ");
    }
}

std::vector<std::string> string_split(const std::string & str, const std::string & delim) {
    int found;
    int end;
    string mystr;
    vector<string> out;
    for (mystr = str; (found = mystr.find(delim)) > -1; mystr = mystr.substr(found + 1)) {
        end = mystr.find(delim);
        auto mysubstr = mystr.substr(0, end);
        if (mysubstr.length() > 0)
            out.push_back(mysubstr);
    }
    if (mystr.length() > 0)
        out.push_back(mystr);
    return out;
}

bool string_contains(const std::string& str, const std::string& text) {
    int res = str.find(text);
    if (res > -1) {
        return true;
    } else return false;
}

std::vector<std::string> genlist(std::string in, std::string key) {
    int keystart = in.find(key);
    if (keystart > -1) {
        string _programs_begin = in.substr(keystart + key.length());
        int end = _programs_begin.find("\n");
        int start = _programs_begin.find("=") + 1;
        string programs = _programs_begin.substr(start, end - start);
        remove_whitespace(programs);
        return string_split(programs, " ");
    } else {
        std::vector<std::string> vec;
        return vec;
    }

}

std::vector<PrefLst> genlistprefix(std::string in, std::string key) {
    
    std::vector<PrefLst> list;
    while (true) {
        int keystart = in.find(key);
        if (keystart > -1) {
            string prefix;
            int pend = keystart;
            int pstart = in.find_last_of("\n",keystart );
            if (pstart == string::npos  ) {
                pstart = 0;
            }
            else { 
                pstart++;
            }
            prefix = in.substr(pstart, pend - pstart);
            

            string _programs_begin = in.substr(keystart + key.length());
            int end = _programs_begin.find("\n");
            int start = _programs_begin.find("=") + 1;
            string programs = _programs_begin.substr(start, end - start);
            
            in = in.substr(end + keystart + key.length());

            remove_whitespace(programs);
            auto prgs = string_split(programs, " ");
            for (auto p: prgs) {
                PrefLst l;
                l.name = p;
                l.prefix = prefix;
                list.push_back(l);
            }

        } else {
            return list;
        }
    }
}



std::string dotconvert(std::string dotname) {
    std::replace(dotname.begin(), dotname.end(), '.', '_');
    return dotname;
}

std::string string_from_file(std::string path) {
    std::ostringstream ostr;
    std::ifstream in(path);
    ostr << in.rdbuf();
    in.close();
    return ostr.str();
}

std::vector<std::string> getconfigslist(string configureac, std::string cfgname) 
{
    int start = configureac.find(cfgname);
    string configfilesbegin = configureac.substr(start);

    string configfiles = extractParen(configfilesbegin);
    configfiles = extractParen2(configfiles);
    remove_whitespace(configfiles);
    return string_split(configfiles, " ");
}

int findClosingParen(std::string text, int openPos, char parentype) 
{
    int closePos = openPos;
    int counter = 1;

    char closing, opening;
    switch (parentype)
    {
    case '(':
    case ')':
        opening = '(';
        closing = ')';
        break;
    case '[':
    case ']':
        opening = '[';
        closing = ']';
        break;

    default:
        break;
    }

    while (counter > 0 ) {
        char c = text[++closePos];
        if (c == opening) {
            counter++;
        }
        else if (c == closing) {
            counter--;
        }
    }
    return closePos;
}

std::string extractParen(std::string text) 
{
    int start = text.find("(");
    int stop = findClosingParen(text, start,')');
    start++;
    string extracted = text.substr(start, stop - start);
    return extracted;
}

std::string extractParen2(std::string text) 
{
    int start, stop;
    while (true)
    {
        start = text.find("[");
        if (start == -1)
            break;
        stop = findClosingParen(text, start, ']');
        if ((stop - start) > 1)  {
            break;
        }
        else {
            text = text.substr(start + 1);
        }
    }
    if (start > -1) {
        start++;
        string extracted = text.substr(start, stop - start);
        return extracted;
    }
    else {
        return string("");
    }
}

std::vector<int> string_findall(std::string str, std::string search) {   
    vector<int> positions; // holds all the positions that sub occurs within str
    size_t pos = str.find(search, 0);
    while(pos != string::npos)
    {
        positions.push_back(pos);
        pos = str.find(search,pos+1);
    }
    return positions;
}


std::string vec2str(std::vector<std::string> vec, std::string sep) {
    string str;
    for (auto iterator = vec.begin();iterator < vec.end(); iterator++) {
        str += *iterator;
        if (iterator < vec.end()-1) {
            str += sep;
        }
    }
    return  str;
}