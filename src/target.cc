#include "target.h"

using namespace autotools;

Target::Target(Makefile& parent): parent(parent) {}

std::string Target::getName() {
    return this->name;
}

std::vector<std::string> Target::getSources() {
    return this->sources;
}

std::vector<std::string> Target::getLDADD() 
{
    return this->ldadd;
}

std::vector<std::string> Target::getLIBADD()
{
    return this->libadd;
}

std::vector<std::string> Target::getCFLAGS()
{
    return this->cflags;
}

std::vector<std::string> Target::getCPPFLAGS() 
{
    return this->cppflags;
}

std::vector<std::string> Target::getLDFLAGS() 
{
    return this->ldflags;
}

Target::Types Target::getType() 
{
    return this->type;
}

void Target::addSources(std::vector<std::string> srcs) 
{
    this->sources.insert(this->sources.end(), srcs.begin(), srcs.end());
}

void Target::addCPPFLAGS(std::vector<std::string> flags) 
{
    this->cppflags.insert(this->cppflags.end(), flags.begin(), flags.end());
}

void Target::clearCFLAGS()
{
    this->cflags.clear();
}

void Target::addCFLAGS(std::vector<std::string> flags)
{
    this->cflags.insert(this->cflags.end(), flags.begin(), flags.end());
}

void Target::clearLDFLAGS()
{
    this->ldflags.clear();
}

void Target::addLDFLAGS(std::vector<std::string> flags) 
{
    this->ldflags.insert(this->ldflags.end(), flags.begin(), flags.end());
}

void Target::clearLDADD()
{
    this->ldadd.clear();
}

void Target::addLDADD(std::vector<std::string> flags)
{
    this->ldadd.insert(this->ldadd.end(), flags.begin(), flags.end() );
}

void Target::clearLIBADD()
{
    this->libadd.clear();
}

void Target::addLIBADD(std::vector<std::string> flags)
{
     this->libadd.insert(this->libadd.end(), flags.begin(), flags.end() );
}

void Target::clearSources() {
    this->sources.clear();
}

void Target::clearCPPFLAGS()
{
    this->cppflags.clear();
}
