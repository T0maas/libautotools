#include "configurator.h"
#include <filesystem>
#include <fstream>
#include <iostream>
#include <stdexcept>
#include <algorithm>
#include "stringops.h"
#include <memory>

using namespace autotools;
using namespace std;

std::shared_ptr<Configurator> Configurator::OpenProject(std::string dir)
{
    std::error_code err;
    std::string config_ac = dir + "/" + "configure.ac";
    if (std::filesystem::is_directory(dir, err))
    {
        if (std::filesystem::is_regular_file(config_ac))
        {
            std::shared_ptr<Configurator> cfg(new Configurator);
            
            cfg->projectpath = dir;
            cfg->configureac = string_from_file(config_ac);
            cfg->getInit();
            cfg->searchCFGs();
            cfg->searchMakefiles();
            cfg->searchCheckLib();
            cfg->searchCheckHeader();
            cfg->searchModules();
            cfg->cxx = cfg->configureac.find("AC_PROG_CXX")!= string::npos  ? true : false;

            for (auto mkf : cfg->makefiles)
            {
                mkf->searchTargets();
                mkf->searchSubdir();
                mkf->searchPkgConfig();
            }
            cfg->searchPCFiles();
            return cfg;
        }
        else
            throw runtime_error("configure.ac not found");
    }
    else
        throw runtime_error("Path not found");
}

std::shared_ptr<Configurator> Configurator::CreateProject(std::string dir, std::string name, std::string version, std::string mail)
{
    std::error_code err;
    while (true) {
        if (std::filesystem::is_directory(dir, err))
        {
            std::shared_ptr<Configurator> cfg(new Configurator);
            cfg->projectpath = dir;
            cfg->name = name;
            cfg->version = version;
            cfg->mail = mail;
            return cfg;
        }
        else
        {
            if (std::filesystem::create_directory(dir))
            {
            /* Configurator cfg;
                cfg.projectpath = dir;
                return cfg;*/
                continue;
            }
            else 
                throw runtime_error("Path can not be used");
        }
    }
}

void Configurator::searchCFGs()
{
    this->cfgs = getconfigslist(this->configureac, "AC_CONFIG_FILES" );
}

void Configurator::searchMakefiles()
{
    for (auto &cfg : cfgs)
    {
        if (string_contains(cfg, "Makefile") && (!string_contains(cfg, ".pc")))
        {
            Makefile_ref mk = make_shared<Makefile>(shared_from_this() , cfg);
            makefiles.push_back(mk);
        }
    }
}

void Configurator::searchPCFiles()
{
    for (auto &cfg : cfgs)
    {
        if (string_contains(cfg, ".pc"))
        {
            PCFile_ref pc = make_shared<PCFile>(*this);
            pc->path = cfg;
            pc->config = cfg + ".in";
            std::fstream file(projectpath +"/"+ pc->config, std::ios::in);
            std::stringstream ss;
            ss << file.rdbuf();
            file.close();
            pc->content = ss.str();
            PCfiles.push_back(pc);
        }
    }
}

void Configurator::searchCheckLib() 
{
    string txt = configureac;
    while (true)
    {
        int start = txt.find("AC_CHECK_LIB");
        if (start == -1)
            break;
        string configfilesbegin = txt.substr(start);
        string configfiles = extractParen(configfilesbegin);
        if (configfiles.length() == 0 )
            break;
        auto list = string_split(configfiles, ",");
        if (list.size() < 4)
            break;
        string found = extractParen2(list[2]);
        string notfound = extractParen2(list[3]);
        Library_check lc{list[0], list[1],found,notfound};
        this->libs.push_back(lc);
        txt = txt.substr(start + 1);
    }
}

void Configurator::getInit() {
    auto found = configureac.find("AC_INIT");
    string init_start = configureac.substr(found);
    string init_in = extractParen(init_start);
    auto splited = string_split(init_in,",");
    if (splited.size() == 3) {
        this->name = extractParen2(splited[0]);
        this->version = extractParen2(splited[1]);
        this->mail = extractParen2(splited[2]);
    }
    
}

void Configurator::searchCheckHeader() {
    auto found = string_findall(configureac,"AC_CHECK_HEADER");

    for (auto & start : found ) {
        string header_start = configureac.substr(start);
        string header_in = extractParen(header_start);
        auto splited = string_split(header_in,",");
        Header_check hc{splited[0],extractParen2(splited[1]), extractParen2(splited[2])};
        this->headers.push_back(hc);
    }

    found = string_findall(configureac,"AC_CHECK_HEADERS");
    for (auto & start : found ) {
        string header_start = configureac.substr(start);
        string header_in = extractParen(header_start);
        auto splited = string_split(header_in,",");
        auto hlist = splited[0];
        auto iffound = splited[1];
        auto ifdnuof = splited[2];
        remove_whitespace(hlist);
        splited = string_split(hlist," ");
        for (auto& he : splited) {
            Header_check hc{he,extractParen2(iffound), extractParen2(ifdnuof)};
            this->headers.push_back(hc);
        }
    }
}

std::vector<Makefile_ref> Configurator::getMakefiles()
{
    return this->makefiles;
}

std::vector<PCFile_ref> Configurator::getPCFiles()
{
    return this->PCfiles;
}

void Configurator::setProjectPath(std::string path)
{
    this->projectpath = path;
}

std::string Configurator::getProjectPath()
{
    return this->projectpath;
}

autotools::Makefile_ref Configurator::addMakefile(std::string path)
{

    auto found = std::find_if(makefiles.begin(), makefiles.end(), [&path](Makefile_ref &mf)
                              { return mf->getPath() == path; });
    if (found != makefiles.end())
    {
        throw runtime_error("This Makefile already exist in specified path");
    }
    Makefile_ref mf ( new Makefile (shared_from_this() ,path));
    this->makefiles.push_back(mf);
    return mf;
}

void Configurator::delMakefile(autotools::Makefile_ref mkf)
{
    auto mkitr = this->makefiles.begin();

    for (; mkitr < this->makefiles.end(); ) {
        if (*mkitr == mkf) {
            mkitr = this->makefiles.erase(mkitr);
        }
        else mkitr++;
    }
}

void Configurator::setVersion(std::string ver) 
{
    this->version = ver;
}

std::string Configurator::getVersion()
{
    return this->version;
}

void Configurator::setName(std::string name) 
{
    this->name = name;
}

std::string Configurator::getName()
{
    return this->name;
}

void Configurator::setMail(std::string mail) 
{
    this->mail = mail;
}

std::string Configurator::getMail()
{
    return  this->mail;
}

void Configurator::setAuxdir(std::string arg) 
{
    this->auxdir = arg;
}

std::string Configurator::getAuxdir()
{
    return this->auxdir;
}

void Configurator::enableCXX(bool tf) 
{
    this->cxx = tf;
}

bool Configurator::getCXX()
{
    return this->cxx;
}

void Configurator::Write()
{
    bool subdirs = false;
    bool ltinit = false;

    for (auto mkf : makefiles)
    {
        if (string_contains(mkf->path, "/"))
            subdirs = true;
        for (auto tg : mkf->getTargets()) {
            switch(tg->getType()) {
                case Target::Types::library:
                    ltinit = true;
                    break;
                default:
                    break;
            }
        }
        mkf->Write();
    }
    for (auto pc : PCfiles) {
        pc->Write();
    }
    this->configureac = "";
    this->configureac += "AC_PREREQ([2.69])\n";
    this->configureac += "AC_INIT([" +  this->name + "], [" + this->version + "], [" + this->mail + "])\n";

    if (auxdir.size() > 0)
        this->configureac += "AC_CONFIG_AUX_DIR(" + this->auxdir + ")\n";

    this->configureac += "AC_CONFIG_MACRO_DIRS([m4])\n";

    if (this->libs.size() > 0){
        for (auto l : libs) {
            this->configureac += "AC_CHECK_LIB(" + l.lib + ", " + l.func + ", [" + l.found + "]" + ", [" + l.notfound + "])\n";
        }
    }

    if (this->headers.size()) {
        for (auto h: headers) {
            if (string_contains(h.header, " "))
                this->configureac += "AC_CHECK_HEADERS(" + h.header +  ", [" + h.found + "]" + ", [" + h.notfound + "])\n";
            else 
                this->configureac += "AC_CHECK_HEADER(" + h.header +  ", [" + h.found + "]" + ", [" + h.notfound + "])\n";
        }
    }


    if (this->modules.size()) {
        for (auto m: modules) {
            this->configureac += "PKG_CHECK_MODULES(" + m.prefix +  ", " + m.name + ")\n";
        }
    }

    if (this->cxx)
        this->configureac += "AC_PROG_CXX\n";

    if (subdirs)
        this->configureac += "AM_INIT_AUTOMAKE([subdir-objects])\n";
    else
        this->configureac += "AM_INIT_AUTOMAKE\n";

    if (this->am_maintainer_mode) {
        this->configureac += "AM_MAINTAINER_MODE([enable])\n";
    }
    
    if (PCfiles.size() > 0)
        this->configureac += "PKG_INSTALLDIR\n";

    if (ltinit)
        this->configureac += "AM_PROG_AR\nLT_INIT\n";

    this->configureac += "AC_CONFIG_FILES([";
    
    std::vector<std::string> mkpc;
    for (auto mk : makefiles)
        mkpc.push_back(mk->path);
    for (auto pc : PCfiles)
        mkpc.push_back(pc->path);

    string mkpc_str = vec2str(mkpc, " ");
    this->configureac += mkpc_str;
    this->configureac += "])\n";


    this->configureac += "AC_OUTPUT\n";
    ofstream out(this->projectpath + "/" + "configure.ac");
    for (auto f: {this->projectpath + "/AUTHORS", this->projectpath + "/NEWS", this->projectpath + "/ChangeLog", this->projectpath + "/README", this->projectpath + "/INSTALL" } )
    {
        ofstream emptyfile(f);
        emptyfile.close();
    }
    out << this->configureac;
    out.close();
}

std::vector<Header_check> Configurator::getCheckHeaders()
{
    return this->headers;
}

std::vector<Library_check> Configurator::getCheckLibraries()
{
    return this->libs;
}

std::vector<Module_check> Configurator::getCheckModules()
{
    return this->modules;
}

Header_check& Configurator::getHeader(int index)
{
    return  this->headers[index];
}

Library_check& Configurator::getLibrary(int index) {
    return this->libs[index];
}

Module_check& Configurator::getModule(int index) {
    return this->modules[index];
}

void Configurator::delHeader(int index) {
    this->headers.erase(this->headers.begin() + index );
}

void Configurator::delLib(int index) {
   this->libs.erase(this->libs.begin() + index );
}

void Configurator::delModule(int index) {
    this->modules.erase(this->modules.begin() + index);
}


void Configurator::addCheckLib(std::string lib, std::string func, std::string found, std::string nfound) {
    this->libs.push_back( {lib,func,found, nfound});
}

void Configurator::addCheckHeader(std::string head, std::string found, std::string nfound) {
    this->headers.push_back({head,found, nfound});
}


autotools::PCFile_ref Configurator::addPCfile(std::string path) {
    auto mypc = make_shared<autotools::PCFile>(*this);
    mypc->path = path;
    mypc->config = path + ".in";
    this->PCfiles.push_back(mypc);
    return mypc;
}

void Configurator::searchModules() {
auto found = string_findall(configureac,"PKG_CHECK_MODULES");

    for (auto & start : found ) {
        string mod_start = configureac.substr(start);
        string mod_in = extractParen(mod_start);
        auto splited = string_split(mod_in,",");
        if (splited.size() == 2)
            this->modules.push_back({splited[0],splited[1]});
    }
}

void Configurator::addModule(string prefix, string name) {
    this->modules.push_back({prefix,name});
}
