#pragma once
#include <string>
namespace autotools {
    class Configurator;

    class PCFile {
        friend class Configurator;
    private:
        autotools::Configurator & parent;
        std::string path, config, content;
    public:
        PCFile(autotools::Configurator & parent);
        std::string getPath();
        std::string getConfig();
        std::string getContent();
        void setContent(std::string str);
        void Write();
    };
}